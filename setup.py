from distutils.core import setup
from setuptools import find_packages

setup(
	name='accabstract',
	version='0.1.2',
	description='Abstraction layer to work with acc file structure',
	author='Genossenschaft Solutionsbüro',
	author_email='info@buero.io',
	url='https://buero.io',
	packages=find_packages(),
	include_package_data=True,
)

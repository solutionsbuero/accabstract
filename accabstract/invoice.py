import uuid
import datetime
from typing import Optional
from pathlib import Path
from accabstract.helpers import StaticHelpers
from accabstract.financialrecord import FinancialRecord


class Invoice(FinancialRecord):

	@classmethod
	def from_dict(cls, project, yamldict: dict):
		invoice = cls(
			project=project,
			unique=yamldict['id'],
			identifier=yamldict['identifier'],
			name=yamldict['name'],
			amount=yamldict['amount'],
			revoked=yamldict['revoked'],
			path=StaticHelpers.path_adjust_to_obj(yamldict['path'], project.directory),
			sendDate=StaticHelpers.optional_date_from_string(yamldict['sendDate']),
			dateOfSettlement=StaticHelpers.optional_date_from_string(yamldict['dateOfSettlement']),
			settlementTransactionId=yamldict['settlementTransactionId']
		)
		return invoice

	@classmethod
	def create_new(
			cls,
			project,
			name: str,
			path: Optional[Path],
			amount: str,
			revoked: bool,
			sendDate: Optional[datetime.date],
			dateOfSettlement: Optional[datetime.date],
			settlementTransactionId: Optional[str],
			identifier: Optional[str] = None
	):
		invoice = cls(
			project=project,
			unique=str(uuid.uuid4()),
			identifier=identifier,
			name=name,
			path=path,
			amount=amount,
			revoked=revoked,
			sendDate=sendDate,
			dateOfSettlement=dateOfSettlement,
			settlementTransactionId=settlementTransactionId
		)
		return invoice

	def __init__(
		self,
		project,
		unique: str,
		identifier: str,
		name: str,
		amount: str,
		revoked: bool,
		path: Optional[Path],
		sendDate: Optional[datetime.date],
		dateOfSettlement: Optional[datetime.date],
		settlementTransactionId: Optional[str]
	):
		FinancialRecord.__init__(self, project, unique, identifier, name, amount, path)
		self.revoked: bool = revoked
		self.sendDate: Optional[datetime.date] = sendDate
		self.dateOfSettlement: Optional[datetime.date] = dateOfSettlement
		self.settlementTransactionId: Optional[str] = settlementTransactionId

	def to_dict(self) -> dict:
		adjusted_path = StaticHelpers.path_adjust_to_yaml(self.path, self.project.directory)
		yamldict = {
			'id': self.id,
			'identifier': self.identifier,
			'name': self.name,
			'amount': self.amount,
			'path': adjusted_path,
			'revoked': self.revoked,
			'sendDate': StaticHelpers.optional_date_to_string(self.sendDate),
			'dateOfSettlement': StaticHelpers.optional_date_to_string(self.dateOfSettlement),
			'settlementTransactionId': self.settlementTransactionId,
			'customerId': self.customer.id,
			'projectId': self.project.id
		}
		return yamldict

import datetime
from typing import List, Optional, Union
from pathlib import Path
from accabstract.constants import SymbolicFolders, SymbolicFilenames
from accabstract.record import Record
from accabstract.organization import Organization
from accabstract.recordmanager import RecordManager
from accabstract.customer import Customer
from accabstract.project import Project
from accabstract.invoice import Invoice
from accabstract.expense import Expense


class ProjectManager(RecordManager):

	def __init__(self, organization: Organization, readonly: bool = True, forceupdate: bool = True):
		RecordManager.__init__(self, organization=organization, readonly=readonly, forceupdate=forceupdate)
		self.projectdir = self.basedir / SymbolicFolders.projects
		self.customers = list()

	def get_customers(self, update=False) -> List[Customer]:
		if self.outdated or self.forceupdate or update:
			cstmrs = list()
			for child in self.projectdir.iterdir():
				customerfile = child / SymbolicFilenames.customer
				if customerfile.is_file():
					customer = Customer.from_yamlfile(customerfile)
					cstmrs.append(customer)
			cstmrs.sort(key=lambda x: x.name.lower(), reverse=False)
			self.customers = cstmrs
			self.outdated = False
		return self.customers

	def get_customer_by_id(self, unique: str) -> Optional[Customer]:
		all_customers = self.get_customers()
		for n in all_customers:
			if n.id == unique:
				return n
		return None

	def get_project_by_id(self, unique: str) -> Optional[Project]:
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				if m.id == unique:
					return m
		return None

	def get_invoice_by_id(self, unique: str) -> Optional[Invoice]:
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				for o in m.invoices:
					if o.id == unique:
						return o
		return None

	def get_expense_by_id(self, unique: str) -> Optional[Expense]:
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				for o in m.expenses:
					if o.id == unique:
						return o
		return None

	def get_next_cid(self) -> str:
		numeric_ids = [0]
		all_customers = self.get_customers()
		for n in all_customers:
			numeric_ids.append(int(''.join(filter(str.isdigit, n.identifier))))
		next_id = max(numeric_ids) + 1
		return "c-{}".format(next_id)

	def get_next_pid(self) -> str:
		numeric_ids = [0]
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				numeric_ids.append(int(''.join(filter(str.isdigit, m.identifier))))
		next_id = max(numeric_ids) + 1
		return "p-{}".format(next_id)

	def get_next_iid(self, yearstr: str):
		numeric_ids = [0]
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				for o in m.invoices:
					parts = o.identifier.split("-")
					if parts[1] == yearstr:
						numeric_ids.append(int(parts[2]))
		next_id = max(numeric_ids) + 1
		return "i-{}-{}".format(yearstr, next_id)

	def get_next_exid(self, yearstr: str):
		numeric_ids = [0]
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				for o in m.expenses:
					parts = o.identifier.split("-")
					if parts[1] == yearstr:
						numeric_ids.append(int(parts[2]))
		next_id = max(numeric_ids) + 1
		return "e-{}-{}".format(yearstr, next_id)

	def get_expense_categories(self) -> list:
		categories = set()
		all_customers = self.get_customers()
		for n in all_customers:
			for m in n.projects:
				for o in m.expenses:
					categories.add(o.expenseCategory)
		return sorted(categories)

	def new_customer(
			self,
			identifier: str,
			name: str,
			street: str = "",
			streetNr: str = 0,
			postalCode: str = 0,
			place: str = " ",
			email: str = ""
	) -> Customer:
		nc = Customer.create_new(
			projectdir=self.projectdir,
			identifier=identifier,
			name=name,
			street=street,
			streetNr=streetNr,
			postalCode=postalCode,
			place=place,
			email=email
		)
		return nc

	def new_project(self, identifier: str, name: str, customer: Optional[Customer] = None, customer_id: str = None) -> Project:
		if customer_id is not None:
			customer = self.get_customer_by_id(customer_id)
		if customer is None:
			raise ValueError("Customer not found")
		np = Project.create_new(
			customer=customer,
			identifier=identifier,
			name=name
		)
		return np

	def new_invoice(
			self,
			identifier: str,
			name: str,
			path: Path,
			amount: str,
			revoked: bool,
			sendDate: Optional[datetime.date],
			dateOfSettlement: Optional[datetime.date],
			settlementTransactionId: Optional[str],
			project: Optional[Project] = None,
			project_id: Optional[str] = None
	) -> Invoice:
		if project_id is not None:
			project = self.get_project_by_id(project_id)
		if project is None:
			raise ValueError("Project not found")
		ni = Invoice.create_new(
			project=project,
			identifier=identifier,
			name=name,
			path=path,
			amount=amount,
			revoked=revoked,
			sendDate=sendDate,
			dateOfSettlement=dateOfSettlement,
			settlementTransactionId=settlementTransactionId
		)
		return ni

	def new_expense(
			self,
			identifier: str,
			name: str,
			path: Path,
			amount: str,
			dateOfAccrual: Optional[datetime.date],
			billable: bool,
			advancedByThirdParty: bool,
			advancedThirdPartyId: str,
			dateOfSettlement: Optional[datetime.date],
			settlementTransactionId: Optional[str],
			expenseCategory: str,
			internal: bool,
			paidWithDebit: bool,
			project: Optional[Project] = None,
			project_id: Optional[str] = None
	) -> Expense:
		if project_id is not None:
			project = self.get_project_by_id(project_id)
		if project is None:
			raise ValueError("Project not found")
		ne = Expense.create_new(
			project=project,
			identifier=identifier,
			name=name,
			path=path,
			amount=amount,
			dateOfAccrual=dateOfAccrual,
			billable=billable,
			advancedByThirdParty=advancedByThirdParty,
			advancedThirdPartyId=advancedThirdPartyId,
			dateOfSettlement=dateOfSettlement,
			settlementTransactionId=settlementTransactionId,
			expenseCategory=expenseCategory,
			internal=internal,
			paidWithDebit=paidWithDebit
		)
		return ne


if __name__ == "__main__":
	pm = ProjectManager(Path("/home/dinu/git/solutionsbuero/sb"))
	c = pm.get_customer_by_id("36bf0c75-8985-4795-b344-2447a84f5d98")
	print(c.name)
	for i in c.projects:
		print(i.name)


import shutil
from abc import ABC, abstractmethod
from accabstract.record import Record
from typing import Optional
from pathlib import Path
from accabstract.helpers import StaticHelpers


class DirectoryRecord(Record, ABC):

	@classmethod
	@abstractmethod
	def from_yamlfile(cls, **kwargs):
		pass

	@staticmethod
	@abstractmethod
	def yaml_filename(directory: Path) -> Path:
		pass

	@abstractmethod
	def to_yaml(self) -> str:
		pass

	@staticmethod
	def create_directory(top: Path, object_name: str) -> Path:
		directory_name = StaticHelpers.name_to_dirname(object_name)
		directory = top / directory_name
		n_exists = 0
		while directory.is_dir():
			directory = top / (directory_name + "_{}".format(n_exists))
			n_exists = n_exists + 1
		directory.mkdir()
		return directory

	def __init__(
		self,
		unique: str,
		identifier: Optional[str],
		name: str,
		directory: Path
	):
		Record.__init__(self, unique, identifier, name)
		self.directory: Path = directory
		self.yamlfile: Path = self.yaml_filename(directory)

	def save(self):
		with self.yamlfile.open("w") as f:
			f.write(self.to_yaml())

	def delete(self):
		shutil.rmtree(self.directory)

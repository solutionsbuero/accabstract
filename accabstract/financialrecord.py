from abc import ABC, abstractclassmethod, abstractmethod, abstractstaticmethod
from pathlib import Path
from accabstract.record import Record
from typing import Optional


class FinancialRecord(Record, ABC):

	@classmethod
	@abstractmethod
	def from_dict(cls, p, yamldict: dict):
		pass

	@abstractmethod
	def to_dict(self) -> dict:
		pass

	def __init__(
		self,
		project,
		unique: str,
		identifier: Optional[str],
		name: str,
		amount: str,
		path: Optional[Path]
	):
		Record.__init__(self, unique, identifier, name)
		self.project = project
		self.customer = project.customer
		self.amount = amount
		self.path = path

	def save(self):
		self.project.save_financial(self)

	def delete(self):
		self.project.remove_financial(self)

import yaml
from typing import List, Optional
from accabstract.organization import Organization
from accabstract.recordmanager import RecordManager
from accabstract.employee import Employee
from accabstract.constants import SymbolicFilenames


class EmployeeManager(RecordManager):

    def __init__(self, organization: Organization, readonly: bool = True, forceupdate: bool = True):
        RecordManager.__init__(self, organization=organization, readonly=readonly, forceupdate=forceupdate)
        self.yamlfile = self.basedir / SymbolicFilenames.employees
        self.employees = list()

    def get_yaml_data(self) -> list:
        with self.yamlfile.open() as f:
            employee_list = yaml.safe_load(f)
        return employee_list

    def get_all(self, update: bool = False) -> List[Employee]:
        if self.outdated or self.forceupdate or update:
            employee_list = list()
            employee_data = self.get_yaml_data()
            for i in employee_data:
                employee_list.append(Employee.from_dict(self.yamlfile, i))
            employee_list.sort(key=lambda x: x.name.lower(), reverse=False)
            self.employees = employee_list
        return self.employees

    def get_employee_by_id(self, unique: str) -> Optional[Employee]:
        all_employees = self.get_all()
        for i in all_employees:
            if i.id == unique:
                return i
        return None

    def get_next_eid(self):
        numeric_ids = [0]
        all_employees = self.get_all()
        for n in all_employees:
            numeric_ids.append(int(''.join(filter(str.isdigit, n.identifier))))
        next_id = max(numeric_ids) + 1
        return "y-{}".format(next_id)

    def new_employee(self, identifier: str, name: str, street: str, streetNr: str, postalCode: str, place: str, email: str) -> Employee:
        e = Employee.create_new(
            yamlfile=self.yamlfile,
            identifier=identifier,
            name=name,
            street=street,
            streetNr=streetNr,
            postalCode=postalCode,
            place=place,
            email=email
        )
        return e

from abc import ABC
from pathlib import Path
from accabstract.record import Record
from accabstract.organization import Organization


class RecordManager(ABC):

	def __init__(self, organization: Organization, readonly: bool = True, forceupdate: bool = True):
		self.basedir = organization.basedir
		self.outdated = True
		self.readonly = readonly
		self.forceupdate = forceupdate

	def save(self, r: Record):
		if self.readonly:
			raise PermissionError("Running in read-only mode")
		else:
			self.outdated = True
			r.save()

	def delete(self, r: Record):
		if self.readonly:
			raise PermissionError("Running in read-only mode")
		else:
			self.outdated = True
			r.delete()

from dataclasses import dataclass


@dataclass(frozen=True)
class SymbolicFilenames:
	organization: str = "acc.yaml"
	employees: str = "employees.yaml"
	customer: str = "customer.yaml"
	project: str = "project.yaml"


@dataclass(frozen=True)
class SymbolicFolders:
	projects: str = "projects"

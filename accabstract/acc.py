from pathlib import Path
from accabstract.projectmanager import ProjectManager
from accabstract.employeemanager import EmployeeManager


class AccAbstraction:

	def __init__(self, basefile: Path):
		self.basefile: Path = basefile
		self.basedir: Path = basefile.parent
		self.pm = ProjectManager(self.basedir)
		self.em = EmployeeManager(self.basedir)

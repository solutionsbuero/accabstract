from typing import Optional
from abc import ABC, abstractmethod


class Record(ABC):

	@classmethod
	@abstractmethod
	def create_new(cls, **kwargs):
		pass

	def __init__(self, unique: str, identifier: Optional[str], name: str):
		self.id: str = unique
		self.identifier: Optional[str] = identifier
		self.name: str = name

	@abstractmethod
	def save(self):
		pass

	@abstractmethod
	def delete(self):
		pass

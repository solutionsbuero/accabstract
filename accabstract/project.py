import yaml
import uuid
from pathlib import Path
from accabstract.constants import SymbolicFilenames
from accabstract.invoice import Invoice
from accabstract.expense import Expense
from accabstract.directoryrecord import DirectoryRecord
from accabstract.financialrecord import FinancialRecord


class Project(DirectoryRecord):

	@classmethod
	def from_yamlfile(cls, customer, yamlfile: Path):
		directory = yamlfile.parent
		with yamlfile.open('r') as f:
			pdata = yaml.safe_load(f)
		project = cls(
			directory=directory,
			customer=customer,
			unique=pdata['project']['id'],
			identifier=pdata['project']['identifier'],
			name=pdata['project']['name']
		)
		for i in pdata['invoices']:
			project.invoices.append(Invoice.from_dict(project, i))
		for i in pdata['expenses']:
			project.expenses.append(Expense.from_dict(project, i))
		return project

	@classmethod
	def create_new(cls, customer, identifier: str, name: str):
		directory = DirectoryRecord.create_directory(customer.directory, name)
		project = cls(
			directory=directory,
			customer=customer,
			unique=str(uuid.uuid4()),
			identifier=identifier,
			name=name
		)
		return project

	@staticmethod
	def yaml_filename(directory: Path) -> Path:
		return directory / SymbolicFilenames.project

	def __init__(
		self,
		directory: Path,
		customer,
		unique: str,
		identifier: str,
		name: str
	):
		DirectoryRecord.__init__(self, unique, identifier, name, directory)
		self.customer = customer
		self.expenses: list = list()
		self.invoices: list = list()

	def to_yaml(self) -> str:
		invoices = list()
		expenses = list()
		for i in self.invoices:
			invoices.append(i.to_dict())
		for i in self.expenses:
			expenses.append(i.to_dict())
		yaml_dict = {
			'project': {
				'id': self.id,
				'identifier': self.identifier,
				'name': self.name,
				'customerId': self.customer.id
			},
			'invoices': invoices,
			'expenses': expenses
		}
		return yaml.safe_dump(yaml_dict)

	def save_financial(self, fr: FinancialRecord):
		if isinstance(fr, Expense):
			to_change = "expenses"
		elif isinstance(fr, Invoice):
			to_change = "invoices"
		else:
			raise TypeError("Unknown financial record type: {}".format(fr.name))
		modify = getattr(self, to_change)
		check = None
		for n, i in enumerate(modify):
			if i.id == fr.id:
				check = n
		if check is None:
			modify.append(fr)
		else:
			modify[check] = fr
		setattr(self, to_change, modify)
		self.save()

	def remove_financial(self, fr: FinancialRecord):
		if isinstance(fr, Expense):
			to_change = "expenses"
		elif isinstance(fr, Invoice):
			to_change = "invoices"
		else:
			raise TypeError("Unknown financial record type: {}".format(fr.name))
		modify = getattr(self, to_change)
		keep = list()
		for i in modify:
			if i.id != fr.id:
				keep.append(i)
		setattr(self, to_change, keep)
		self.save()

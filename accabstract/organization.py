import yaml
from typing import Optional
from pathlib import Path
from accabstract.constants import SymbolicFilenames, SymbolicFolders
from accabstract.helpers import StaticHelpers


class Organization:

	@classmethod
	def from_basedir(cls, basedir: Path, name: Optional[str] = None):
		yamlfile = Path(basedir) / SymbolicFilenames.organization
		if not yamlfile.is_file():
			Organization.initialize(basedir)
		with yamlfile.open() as f:
			alldata = yaml.safe_load(f)
			yamldata = alldata['company']
		if name is None:
			name = yamldata.get("name", "")
		return cls(
			basedir=basedir,
			name=name,
			street=yamldata.get("street", ""),
			streetNr=yamldata.get("streetNr", ""),
			postalCode=yamldata.get("postalCode", ""),
			place=yamldata.get("place", ""),
			phone=yamldata.get("phone", ""),
			mail=yamldata.get("mail", ""),
			url=yamldata.get("url", ""),
			logo=yamldata.get("logo", "")
		)

	def __init__(
		self,
		basedir: Path,
		name: str,
		street: str = "",
		streetNr: str = "",
		postalCode: str = "",
		place: str = "",
		phone: str = "",
		mail: str = "",
		url: str = "",
		logo: str = ""
	):
		self.basedir: Path = Path(basedir)
		self.yamlfile = self.basedir / SymbolicFilenames.organization
		self.name: str = name
		self.street: str = street
		self.streetNr: str = streetNr
		self.postalCode: str = postalCode
		self.place: str = place
		self.phone: str = phone
		self.mail: str = mail
		self.url: str = url
		self.logo: Path = self.basedir / logo

	@staticmethod
	def initialize(basedir: Path):
		yamlfile = basedir / SymbolicFilenames.organization
		if not yamlfile.is_file():
			init_org = {
				"company": {},
				"journalConfig": {},
				"currency": ""
			}
			with yamlfile.open(mode='w') as f:
				f.write(yaml.safe_dump(init_org))
		epath = basedir / SymbolicFilenames.employees
		if not epath.is_file():
			with epath.open(mode='w') as f:
				f.write(yaml.safe_dump([]))
		ppath = basedir / SymbolicFolders.projects
		if not ppath.is_dir():
			ppath.mkdir()

	def company_to_dict(self) -> dict:
		return {
			"name": self.name,
			"street": self.street,
			"streetNr": self.streetNr,
			"postalCode": self.postalCode,
			"phone": self.phone,
			"mail": self.mail,
			"url": self.url,
			"logo": StaticHelpers.path_adjust_to_yaml(self.logo, self.basedir)
		}

	def save(self):
		self.initialize(self.basedir)
		with self.yamlfile.open("r") as f:
			yamldata = yaml.safe_load(f.read())
		yamldata["company"] = self.company_to_dict()
		with self.yamlfile.open("w") as g:
			g.write(yaml.safe_dump(yamldata))

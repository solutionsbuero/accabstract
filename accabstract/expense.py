import uuid
import datetime
from typing import Optional
from pathlib import Path
from accabstract.helpers import StaticHelpers
from accabstract.financialrecord import FinancialRecord


class Expense(FinancialRecord):

    @classmethod
    def from_dict(cls, project, yamldict: dict):
        expense = cls(
            project=project,
            unique=yamldict['id'],
            identifier=yamldict['identifier'],
            name=yamldict['name'],
            amount=yamldict['amount'],
            path=StaticHelpers.path_adjust_to_obj(yamldict['path'], project.directory),
            dateOfAccrual=StaticHelpers.optional_date_from_string(yamldict['dateOfAccrual']),
            billable=yamldict['billable'],
            advancedByThirdParty=yamldict['advancedByThirdParty'],
            advancedThirdPartyId=yamldict['advancedThirdPartyId'],
            dateOfSettlement=StaticHelpers.optional_date_from_string(yamldict['dateOfSettlement']),
            settlementTransactionId=yamldict['settlementTransactionId'],
            expenseCategory=yamldict['expenseCategory'],
            internal=yamldict['internal'],
            paidWithDebit=yamldict['paidWithDebit']
        )
        return expense

    @classmethod
    def create_new(
        cls,
        project,
        name: str,
        path: Optional[Path],
        amount: str,
        dateOfAccrual: Optional[datetime.date],
        billable: bool,
        advancedByThirdParty: bool,
        advancedThirdPartyId: Optional[str],
        dateOfSettlement: Optional[datetime.date],
        settlementTransactionId: Optional[str],
        expenseCategory: str,
        internal: bool,
        paidWithDebit: bool,
        identifier: str
    ):
        expense = cls(
            project=project,
            unique=str(uuid.uuid4()),
            identifier=identifier,
            name=name,
            path=path,
            amount=amount,
            dateOfAccrual=dateOfAccrual,
            billable=billable,
            advancedByThirdParty=advancedByThirdParty,
            advancedThirdPartyId=advancedThirdPartyId,
            dateOfSettlement=dateOfSettlement,
            settlementTransactionId=settlementTransactionId,
            expenseCategory=expenseCategory,
            internal=internal,
            paidWithDebit=paidWithDebit
        )
        return expense

    def __init__(
            self,
            project,
            unique: str,
            name: str,
            amount: str,
            path: Optional[Path],
            dateOfAccrual: Optional[datetime.date],
            billable: bool,
            advancedByThirdParty: bool,
            advancedThirdPartyId: Optional[str],
            dateOfSettlement: Optional[datetime.date],
            settlementTransactionId: Optional[str],
            expenseCategory: str,
            internal: bool,
            paidWithDebit: bool,
            identifier: str
    ):
        FinancialRecord.__init__(self, project, unique, identifier, name, amount, path)
        self.dateOfAccrual: Optional[datetime.date] = dateOfAccrual
        self.billable: bool = billable
        self.advancedByThirdParty: bool = advancedByThirdParty
        self.advancedThirdPartyId: Optional[str] = advancedThirdPartyId
        self.dateOfSettlement: Optional[datetime.date] = dateOfSettlement
        self.settlementTransactionId: Optional[str] = settlementTransactionId
        self.expenseCategory: str = expenseCategory
        self.internal: bool = internal
        self.paidWithDebit: bool = paidWithDebit

    def to_dict(self) -> dict:
        adjusted_path = StaticHelpers.path_adjust_to_yaml(self.path, self.project.directory)
        yamldict = {
            'id': self.id,
            'identifier': self.identifier,
            'name': self.name,
            'amount': self.amount,
            'path': adjusted_path,
            'dateOfAccrual': StaticHelpers.optional_date_to_string(self.dateOfAccrual),
            'billable': self.billable,
            'obligedCustomerId': self.project.customer.id,
            'advancedByThirdParty': self.advancedByThirdParty,
            'advancedThirdPartyId': self.advancedThirdPartyId,
            'dateOfSettlement': StaticHelpers.optional_date_to_string(self.dateOfSettlement),
            'settlementTransactionId': self.settlementTransactionId,
            'expenseCategory': self.expenseCategory,
            'paidWithDebit': self.paidWithDebit,
            'internal': self.internal,
            'projectId': self.project.id
        }
        return yamldict

import uuid
import yaml
from pathlib import Path
from accabstract.record import Record


class Employee(Record):

    @classmethod
    def from_dict(cls, yamlfile: Path, edict: dict):
        employee = cls(
            yamlfile=yamlfile,
            unique=edict['id'],
            identifier=edict['identifier'],
            name=edict['name'],
            street=edict['street'],
            streetNr=str(edict['streetNr']),
            postalCode=str(edict['postalCode']),
            place=edict['place'],
            email=edict.get("email", ""),
        )
        return employee

    @classmethod
    def create_new(cls, yamlfile, identifier: str, name: str, street: str = "", streetNr: str = "", postalCode: str = "", place: str = "", email: str = ""):
        employee = cls(
            yamlfile=yamlfile,
            unique=str(uuid.uuid4()),
            identifier=identifier,
            name=name,
            street=street,
            streetNr=streetNr,
            postalCode=postalCode,
            place=place,
            email=email
        )
        return employee

    def __init__(self, yamlfile: Path, unique: str, identifier: str, name: str, street: str, streetNr: str, postalCode: str, place: str, email: str, partyType: int=0):
        Record.__init__(self, unique, identifier, name)
        self.yamlfile: Path = yamlfile
        self.street: str = street
        self.streetNr: str = streetNr
        self.postalCode: str = postalCode
        self.place: str = place
        self.email: str = email
        self.partyType: int = partyType


    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'identifier': self.identifier,
            'name': self.name,
            'street': self.street,
            'streetNr': self.streetNr,
            'postalCode': self.postalCode,
            'place': self.place,
            'email': self.email,
            'partyType': self.partyType
        }

    def save(self):
        with self.yamlfile.open() as f:
            edata = yaml.safe_load(f)
        check = None
        for n, i in enumerate(edata):
            if self.id == i["id"]:
                check = n
        if check is None:
            edata.append(self.to_dict())
        else:
            edata[check] = self.to_dict()
        with self.yamlfile.open('w') as g:
            g.write(yaml.safe_dump(edata))

    def delete(self):
        keep = list()
        with self.yamlfile.open() as f:
            edata = yaml.safe_load(f)
        for i in edata:
            if self.id != i["id"]:
                keep.append(i)
        with self.yamlfile.open('w') as g:
            g.write(yaml.safe_dump(keep))

import re
import os
import datetime
from pathlib import Path
from typing import Optional, Union


class StaticHelpers:

	CONSIDERED_EMPTY = ["", " ", None]

	@staticmethod
	def name_to_dirname(name: str) -> str:
		splited = name.split(" ")
		parts = list()
		for i in splited:
			parts.append(re.sub('[^A-Za-z0-9]+', '', i).lower())
		return "-".join(parts)

	@staticmethod
	def optional_date_to_string(dateobj: Optional[datetime.date]) -> Optional[str]:
		if dateobj is None:
			return None
		else:
			return dateobj.isoformat()

	@staticmethod
	def optional_date_from_string(datestr: str) -> Optional[datetime.date]:
		if datestr in StaticHelpers.CONSIDERED_EMPTY:
			return None
		else:
			try:
				return datetime.date.fromisoformat(datestr)
			except ValueError:
				return None

	@staticmethod
	def path_adjust_to_yaml(assetpath: Union[None, Path, str], basedir: Union[Path, str]) -> Optional[str]:
		if assetpath is None or assetpath == "":
			return None
		return os.path.relpath(str(assetpath), str(basedir))

	@staticmethod
	def path_adjust_to_obj(assetpath: Union[str, Path, None], projectpath: Path) -> Optional[Path]:
		if assetpath in StaticHelpers.CONSIDERED_EMPTY:
			return None
		return projectpath / assetpath

if __name__ == "__main__":
	print(StaticHelpers.optional_date_from_string("07-07"))

import yaml
import uuid
import shutil
from typing import List, Optional
from pathlib import Path
from accabstract.constants import SymbolicFilenames
from accabstract.project import Project
from accabstract.helpers import StaticHelpers
from accabstract.directoryrecord import DirectoryRecord


class Customer(DirectoryRecord):

	@classmethod
	def from_yamlfile(cls, yamlfile: Path):
		directory = yamlfile.parent
		with yamlfile.open('r') as f:
			cdata = yaml.safe_load(f)
		customer = cls(
			directory=directory,
			unique=cdata['id'],
			identifier=cdata['identifier'],
			name=cdata['name'],
			street=cdata['street'],
			streetNr=str(cdata['streetNr']),
			postalCode=str(cdata['postalCode']),
			place=cdata['place'],
			email=cdata.get("email", "")
		)
		return customer

	@classmethod
	def create_new(cls, projectdir: Path, identifier: str, name: str, street: str = '', streetNr: str = "", postalCode: str = "", place: str = "", email: str = ""):
		directory = DirectoryRecord.create_directory(projectdir, name)
		customer = cls(
			directory=directory,
			unique=str(uuid.uuid4()),
			identifier=identifier,
			name=name,
			street=street,
			streetNr=streetNr,
			postalCode=postalCode,
			place=place,
			email=email
		)
		return customer

	@staticmethod
	def yaml_filename(directory: Path) -> Path:
		return directory / SymbolicFilenames.customer

	def __init__(
		self,
		unique: str,
		identifier: str,
		name: str,
		directory: Path,
		street: str = "",
		streetNr: str = "",
		postalCode: str = "",
		place: str = '',
		email: str = "",
		partyType: int = 1
	):
		DirectoryRecord.__init__(self, unique, identifier, name, directory)
		self.street: str = street
		self.streetNr: str = streetNr
		self.postalCode: str = postalCode
		self.place: str = place
		self.email: str = email
		self.partyType: int = partyType
		self.projects: List[Project] = self.collect_projects()

	def collect_projects(self) -> List[Project]:
		projects = list()
		if not self.yamlfile.exists():
			return []
		for child in self.directory.iterdir():
			projectfile = child / SymbolicFilenames.project
			if projectfile.is_file():
				p = Project.from_yamlfile(self, projectfile)
				projects.append(p)
		return projects

	def to_yaml(self) -> str:
		yaml_dict = {
			'id': self.id,
			'identifier': self.identifier,
			'name': self.name,
			'street': self.street,
			'streetNr': self.streetNr,
			'postalCode': self.postalCode,
			'place': self.place,
			'email': self.email,
			'partyType': self.partyType
		}
		return yaml.safe_dump(yaml_dict)
